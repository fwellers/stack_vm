# Stack VM

## Instruction Set

The VM has a 16 Bit Instruction Set.
The first 8 bit determine the instruction, while the last 8 bit contain the data.


### PUSH

`0000 0001`
Push the provided value on the stack


### POP

`0000 0010`
Remove the last value from the stack


### ADD

`0000 0011`
Add the last two values on the stack and push the result on the stack


### SUB

`0000 0100`
Substract the last two values from each other and push the result on the stack


### No Function

0000 0000 
...

