use crate::stack;

struct A<'a> {
    action: &'a dyn Fn(u8),
}

#[test]
fn test_struct() {
    let t = A {
        action: &|x| println!("{:?}", x),
    };
    let methd: &dyn Fn(u8) -> u8 = &|x| x * x;
    assert_eq!(methd(2), 4);
    (t.action)(2);
}

#[test]
fn stack_multiple_values() {
    let mut stack: stack::Stack<u16> = stack::Stack::default();
    stack.push(1);
    stack.push(2);
    stack.push(3);
    stack.push(4);
    assert_eq!(stack.pop().unwrap(), 4);
    assert_eq!(stack.pop().unwrap(), 3);
    assert_eq!(stack.pop().unwrap(), 2);
    assert_eq!(stack.pop().unwrap(), 1);
    assert!(stack.pop().is_none());
}

#[test]
fn stack_peek() {
    let mut stack: stack::Stack<u16> = stack::Stack::default();
    stack.push(1);
    stack.push(2);
    stack.push(3);
    stack.push(4);
    assert_eq!(stack.peek().unwrap(), 4);
    assert_eq!(stack.pop().unwrap(), 4);
    assert_eq!(stack.pop().unwrap(), 3);
    assert_eq!(stack.peek().unwrap(), 2);
    assert_eq!(stack.peek().unwrap(), 2);
    assert_eq!(stack.pop().unwrap(), 2);
    assert_eq!(stack.peek().unwrap(), 1);
    assert_eq!(stack.pop().unwrap(), 1);
    assert!(stack.peek().is_none());
}
