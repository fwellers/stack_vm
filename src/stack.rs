#[derive(Clone)]
struct StackNode<T> {
    data: T,
    next: Option<Box<StackNode<T>>>,
}

pub struct Stack<T>
where
    T: Copy,
{
    head: Option<Box<StackNode<T>>>,
    size: u16,
}

impl<T> Default for Stack<T>
where
    T: Copy,
{
    fn default() -> Stack<T> {
        Stack {
            head: None,
            size: 0,
        }
    }
}

impl<T> Stack<T>
where
    T: Copy,
{
    pub fn push(&mut self, item: T) {
        let head = self.head.clone();
        self.size += 1;
        self.head = Some(Box::new(StackNode {
            data: item,
            next: head,
        }))
    }
    pub fn pop(&mut self) -> Option<T> {
        match self.head.clone() {
            Some(head) => {
                if self.size > 0 {
                    self.size -= 1;
                    let next = head.next;
                    match next {
                        Some(x) => self.head = Some(x),
                        None => self.head = None,
                    }
                    return Some(head.data);
                } else {
                    return None;
                }
            }
            None => return None,
        }
    }
    pub fn peek(&self) -> Option<T> {
        if self.size > 0 {
            match self.head.clone() {
                Some(head) => return Some(head.data),
                None => return None,
            }
        } else {
            return None;
        }
    }
}
