#![allow(dead_code, unused_variables)]
mod stack;

#[cfg(test)]
mod tests;

fn main() {
    let mut my_stack: stack::Stack<u16> = stack::Stack::default();
    my_stack.push(1);
    my_stack.push(2);
    my_stack.push(3);
    my_stack.push(4);

    println!("{:?}", my_stack.pop());
    println!("{:?}", my_stack.pop());
    println!("{:?}", my_stack.peek());
    println!("{:?}", my_stack.pop());
    println!("{:?}", my_stack.pop());
    let mut my_program: Vec<u16> = Vec::default();

    my_program.push(0x0104);
    my_program.push(0x0103);
    my_program.push(0x0400);
    my_program.push(0x0500);

    let mut vm: StackVm = StackVm::default();
    vm.run(my_program);
}

enum InstructionName {
    PUSH,
    POP,
    ADD,
    SUB,
    PRINT,
}

impl std::fmt::Display for InstructionName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let name: &str;
        match self {
            InstructionName::PUSH => name = "push",
            InstructionName::POP => name = "pop",
            InstructionName::ADD => name = "add",
            InstructionName::SUB => name = "sub",
            InstructionName::PRINT => name = "print",
        }
        write!(f, "{}", name)
    }
}

struct Instruction<'a> {
    name: InstructionName,
    mask: u16,
    pattern: u16,
    action: &'a dyn Fn(&mut stack::Stack<u16>, u16),
}

impl<'a> Instruction<'a> {
    fn match_other(&self, other: u16) -> bool {
        (other & self.mask) == self.pattern
    }
}

impl<'a> Default for StackVm<'a> {
    fn default() -> Self {
        let mut map: std::collections::HashMap<u16, Instruction> =
            std::collections::HashMap::default();
        map.insert(
            0x1,
            Instruction {
                name: InstructionName::PUSH,
                mask: 0x0100,
                pattern: 0x0100,
                action: &|stack, parameter| stack.push(parameter),
            },
        );
        map.insert(
            0x2,
            Instruction {
                name: InstructionName::POP,
                mask: 0x0200,
                pattern: 0x0200,
                action: &|stack, parameter| {
                    stack.pop();
                    ()
                },
            },
        );
        map.insert(
            0x3,
            Instruction {
                name: InstructionName::ADD,
                mask: 0x0300,
                pattern: 0x0300,
                action: &|stack, parameter| {
                    stack.pop().and_then(|x: u16| -> Option<u16> {
                        x.checked_add(stack.pop().unwrap_or(0)).and_then(|y| {
                            stack.push(y);
                            return None;
                        })
                    });
                },
            },
        );
        map.insert(
            0x4,
            Instruction {
                name: InstructionName::SUB,
                mask: 0x0400,
                pattern: 0x0400,
                action: &|stack, parameter| {
                    stack.pop().and_then(|x: u16| -> Option<u16> {
                        stack.pop().unwrap_or(0).checked_sub(x).and_then(|y| {
                            stack.push(y);
                            return None;
                        })
                    });
                },
            },
        );
        map.insert(
            0x5,
            Instruction {
                name: InstructionName::PRINT,
                mask: 0x0500,
                pattern: 0x0500,
                action: &|stack, parameter| println!("PRINT: {}", stack.pop().unwrap_or(0)),
            },
        );

        return StackVm {
            stack: stack::Stack::default(),
            instructions: map,
        };
    }
}

/*
 * Instruction Set (16 bit instructions)
 * PREFIX       INSTRUCTION     DESCRIPTION
 * 0000 0000
 * 0000 0001     PUSH            push a value on the stack, use like "PUSH 5"
 * 0000 0010     POP             remove a value from the stack, use like "POP 5"
 * 0000 0011     ADD             add the last two values on the stack and push the result "ADD"
 * 0000 0100     SUB             subtract the last value from the stack from value before that one
 *
 * 0000 0111     PRINT           print the top value of the stack
 * 0000 0110
 * 0000 1111
 *
 *
 *
 */

struct StackVm<'a> {
    stack: stack::Stack<u16>,
    instructions: std::collections::HashMap<u16, Instruction<'a>>,
}

impl<'a> StackVm<'a> {
    fn run(&mut self, program: Vec<u16>) {
        for i in program {
            self.execute_instruction(i);
        }
    }

    fn execute_instruction(&mut self, instruction: u16) {
        let param: u16 = instruction & 0x00FF;
        match self.instructions.get(&(instruction >> 8)) {
            Some(x) => {
                println!("instruction: {}", x.name);
                (x.action)(&mut self.stack, param);
                ()
            }
            None => (),
        }
    }
}
